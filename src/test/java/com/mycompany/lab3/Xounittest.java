/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class Xounittest {

    public Xounittest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlay_Output_X() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayRow1_Output_O() {
        char[][] board = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayRow2_Output_X() {
        char[][] board = {{'O', 'O', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayRow3_Output_X() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayCol1_Output_X() {
        char[][] board = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayCol2_Output_X() {
        char[][] board = {{'-', 'X', '-'}, {'O', 'X', '-'}, {'-', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayCol3_Output_X() {
        char[][] board = {{'-', 'O', 'X'}, {'O', 'X', 'X'}, {'O', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinPlayX1_Output_X() {
        char[][] board = {{'X', 'O', '-'}, {'O', 'X', 'X'}, {'O', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }

    @Test

    public void testCheckWinPlayX2_Output_X() {
        char[][] board = {{'O', 'O', 'X'}, {'O', 'X', 'X'}, {'X', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testCheckDrawPlay_Output_Draw() {
        char[][] board = {{'X', 'O', 'X'}, {'X', 'O', 'X'}, {'O', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, XOProgram.checkDraw(board, currentPlayer));
    }

}
